<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Config\Definition\Exception\Exception;
use App\Manager\ExceptionManager;

class WeatherService
{
    private $client;
    private $apiKey;
    private $apiBaseUrl;

    public function __construct($apiKey)
    {
        $this->client = HttpClient::create();
        $this->apiKey = $apiKey;
        $this->apiBaseUrl = 'https://api.darksky.net/forecast/';
    }

    /**
     * @return array
     * @param  string $latitude
     * @param  string $longitude
     */
    public function getWeather($latitude = '43.6042600', $longitude = '1.4436700')
    {
        // darsky api call
        try {
            $weatherApiResponse = $this->client->request('GET', $this->apiBaseUrl . $this->apiKey . '/'. $latitude . ',' . $longitude . '?lang=fr&units=si');
            $statusCode = $weatherApiResponse->getStatusCode();
            // handle response
            if ($statusCode === 200) {
                if (empty($weatherApiResponse->getContent())) {
                    ExceptionManager::throwEmptyResponseException();
                }
                $currentWeather = json_decode($weatherApiResponse->getContent())->currently;
                $temperature = round($currentWeather->temperature, 1);
                // convert windSpeed from m/s to km/H
                $windSpeed = round($currentWeather->windSpeed * 3.6, 1);
                return [
                    'temperature' => $temperature, // en °C
                    'vent' => $windSpeed // en km/H
                ];
            }
            else {
                ExceptionManager::throwExceptionFromStatusCode($statusCode);
            }
        }
        catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
