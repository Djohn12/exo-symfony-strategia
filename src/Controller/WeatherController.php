<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\WeatherService;
use Symfony\Component\HttpFoundation\Response;

class WeatherController extends AbstractController
{
    private $wheatherService;

    public function __construct(WeatherService $weather)
    {
        $this->weatherService = $weather;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $defaultCity = 'Toulouse';
        $data = $this->weatherService->getWeather();
        if (is_array($data)) return $this->render('weather/index.html.twig', array('data' => $data, 'location' => $defaultCity));
        else return $this->render('weather/error.html.twig', array('data' => $data));
    }

    /**
     * @Route("/weather/{location}/{latitude}/{longitude}", name="searchedWeather", methods={"GET"})
     * @param  string $location
     * @param  string $latitude
     * @param  string $longitude
     */
    public function getweather($location, $latitude, $longitude)
    {
        $city = explode(',', $location)[0];
        $data = $this->weatherService->getWeather($latitude, $longitude);
        return $this->render('weather/weather.html.twig', array('data' => $data, 'location' => $city));
    }

    /**
     * @Route("/{any}", name="errorPage", methods={"GET"})
     */
    public function catch()
    {
        return $this->render('weather/404.html.twig');
    }
}
