<?php

namespace App\Manager;

use Symfony\Component\Config\Definition\Exception\Exception;

class ExceptionManager {

	/**
	 * @param  [int] $statusCode
	 */
	public static function throwExceptionFromStatusCode($statusCode) {
		if ($statusCode === 400) throw new Exception('Request Malformed');
		if ($statusCode === 401) throw new Exception('Unauthorized Access');
		if ($statusCode === 403) throw new Exception ('Permission Denied');
		if ($statusCode === 405) throw new Exception('Method Not Allowed');
		if ($statusCode === 500) throw new Exception('Internal Servor Error');
		throw new Exception('Unknown Error');
	}

	public static function throwEmptyResponseException() {
		throw new Exception('Response Content is empty');
	}
}